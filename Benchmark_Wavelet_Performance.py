import subprocess 
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt

rcParams = {'font.size': 20 , 'font.weight': 'normal', 
            'font.family': 'sans-serif',
            'axes.unicode_minus':False, 
            'axes.labelweight':'normal'}
plt.rcParams.update(rcParams)

letters = ['A','B','C','D','E','F','G','H','I','J', 'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
prog_path = './benchmark_wavelet_tree.out'

def Return_Random_String(alphabet_size, random_string_length):
	alphabet_string = ''.join(letters[:alphabet_size])
	random_string = ''
	for i in range(random_string_length):
		random_string += random.choice(alphabet_string)
	alpha_counts = dict()
	for i in range(alphabet_size):
		alpha_counts[letters[i]] = random_string.count(letters[i])
	return random_string, alpha_counts
	
alphabet_size = [4,6,8,12,16,20,24]
random_string_length = [10, 50, 100, 200, 300, 500, 700, 900, 1000, 2000, 3000, 7500, 10000, 25000]

number_rank_queries = 10
number_select_queries = 10
output = []

for a in alphabet_size:
	for str_len in random_string_length:
		Rand_Str, char_counts = Return_Random_String(a, str_len)
		alphabet_string = ''.join(letters[:a])
		command = prog_path +' '+str(2*number_rank_queries+2)+' Rank '+Rand_Str+' '
		for i in range(number_rank_queries):
			query_char = random.choice(alphabet_string)
			index_pos = random.randint(0,str_len)
			command+='0'+str(index_pos) +' '+query_char+' '
		p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		retval = p.wait(3000)
		for line in p.stdout.readlines():
			output.append(str(line).split(' '))
			print (str(line))


		command = prog_path +' '+str(2*number_rank_queries+2)+' Select '+Rand_Str+' '
		for i in range(number_select_queries):
			rank = -1
			while(rank <= 0):
				query_char = random.choice(alphabet_string)
				rank = char_counts[query_char]
			rank = random.randint(1, rank)
			command+='0'+str(rank) +' '+query_char+' '	
		p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		for line in p.stdout.readlines():
			output.append(str(line).split(' '))
			print (str(line))

df = pd.DataFrame(output, columns = ['Operation','Time', 'Length', 'Alphabet Size'])
df['Operation'] = df['Operation'].str.strip('b\'')
df['Alphabet Size'] = df['Alphabet Size'].str.strip('\'')
df['Alphabet Size'] = df['Alphabet Size'].astype(float)
df['Length'] = df['Length'].astype(float)
df['Time'] = df['Time'].astype(float)

df_fixed_alpha = df[(df['Alphabet Size'] == 4) |
					(df['Alphabet Size'] == 12) | 
					(df['Alphabet Size'] == 24)]
df_fixed_alpha.sort_values(by = ['Length'], inplace = True)

df_fixed_lengths = df[(df['Length'] == 500) | 
					  (df['Length'] == 7500) | 
					  (df['Length'] == 25000)]
df_fixed_lengths.sort_values(by = ['Alphabet Size'], inplace = True)

fig_fixed_alpha_rank, ax_fixed_alpha_rank = plt.subplots(1,1,figsize = (16,10))
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 4) & 
(df_fixed_alpha['Operation'] == 'Rank')].plot('Length', 'Time', color = 'red', marker = 'o', ms = 8, ax = ax_fixed_alpha_rank)
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 12) & 
(df_fixed_alpha['Operation'] == 'Rank')].plot('Length', 'Time', color = 'blue', marker = 'o', ms = 8, ax = ax_fixed_alpha_rank)
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 24) & 
(df_fixed_alpha['Operation'] == 'Rank')].plot('Length', 'Time', color = 'green', marker = 'o', ms = 8, ax = ax_fixed_alpha_rank)
ax_fixed_alpha_rank.legend(['Alphabet size = 4', 'Alphabet size = 12', 'Alphabet size = 24'])
ax_fixed_alpha_rank.set_ylabel('Time(S)')
ax_fixed_alpha_rank.set_title('Query Time(S) for 10 Rank Operations on Strings of Varying Lengths')
ax_fixed_alpha_rank.set_xlim([-25, 25200])
fig_fixed_alpha_rank.tight_layout()

fig_fixed_alpha_sel, ax_fixed_alpha_sel = plt.subplots(1,1,figsize = (16,10))
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 4) & 
(df_fixed_alpha['Operation'] == 'Select')].plot('Length', 'Time', color = 'red', marker = 'o', ms = 8, ax = ax_fixed_alpha_sel)
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 12) & 
(df_fixed_alpha['Operation'] == 'Select')].plot('Length', 'Time', color = 'blue', marker = 'o', ms = 8, ax = ax_fixed_alpha_sel)
df_fixed_alpha[(df_fixed_alpha['Alphabet Size'] == 24) & 
(df_fixed_alpha['Operation'] == 'Select')].plot('Length', 'Time', color = 'green', marker = 'o', ms = 8, ax = ax_fixed_alpha_sel)
ax_fixed_alpha_sel.legend(['Alphabet size = 4', 'Alphabet size = 12', 'Alphabet size = 24'])
ax_fixed_alpha_sel.set_ylabel('Time(S)')
ax_fixed_alpha_sel.set_title('Query Time(S) for 10 Select Operations on Strings of Varying Lengths')
ax_fixed_alpha_sel.set_xlim([-25, 25200])
fig_fixed_alpha_sel.tight_layout()

fig_fixed_len_rank, ax_fixed_len_rank = plt.subplots(1,1,figsize = (16,10))
df_fixed_lengths[(df_fixed_lengths['Length'] == 500) & 
(df_fixed_lengths['Operation'] == 'Rank')].plot('Alphabet Size', 'Time', color = 'red', marker = 'o', ms = 8, ax = ax_fixed_len_rank)
df_fixed_lengths[(df_fixed_lengths['Length'] == 7500) & 
(df_fixed_lengths['Operation'] == 'Rank')].plot('Alphabet Size', 'Time', color = 'blue', marker = 'o', ms = 8, ax = ax_fixed_len_rank)
df_fixed_lengths[(df_fixed_lengths['Length'] == 25000) & 
(df_fixed_lengths['Operation'] == 'Rank')].plot('Alphabet Size', 'Time', color = 'green', marker = 'o', ms = 8, ax = ax_fixed_len_rank)
ax_fixed_len_rank.legend(['String Length = 500', 'String Length = 7500', 'String Length = 25000'])
ax_fixed_len_rank.set_ylabel('Time(S)')
ax_fixed_len_rank.set_title('Query Time(s) for 10 Rank Operations on Strings of Varying Alphabet Sizes')
ax_fixed_len_rank.set_xlim([2, 25])
fig_fixed_len_rank.tight_layout()

fig_fixed_len_sel, ax_fixed_len_sel = plt.subplots(1,1,figsize = (16,10))
df_fixed_lengths[(df_fixed_lengths['Length'] == 500) & 
(df_fixed_lengths['Operation'] == 'Select')].plot('Alphabet Size', 'Time', color = 'red', marker = 'o', ms = 8, ax = ax_fixed_len_sel)
df_fixed_lengths[(df_fixed_lengths['Length'] == 7500) & 
(df_fixed_lengths['Operation'] == 'Select')].plot('Alphabet Size', 'Time', color = 'blue', marker = 'o', ms = 8, ax = ax_fixed_len_sel)
df_fixed_lengths[(df_fixed_lengths['Length'] == 25000) & 
(df_fixed_lengths['Operation'] == 'Select')].plot('Alphabet Size', 'Time', color = 'green', marker = 'o', ms = 8, ax = ax_fixed_len_sel)
ax_fixed_len_sel.legend(['String Length = 500', 'String Length = 7500', 'String Length = 25000'])
ax_fixed_len_sel.set_ylabel('Time(S)')
ax_fixed_len_sel.set_title('Query Time(S) for 10 Select Operations on Strings of Varying Alphabet Sizes')
ax_fixed_len_sel.set_xlim([2, 25])
fig_fixed_len_sel.tight_layout()

plt.show()

print (df)