#include"Wavelet.h"
#include <chrono>
#include <ctime>
#include<cstdlib>
#include"fstream"
#include"map"
#include <sstream>  

using namespace std;

void Write_Wavelet_Object(node* T, string tracker, ofstream& o)
{
	if(T != NULL)
	{
		o<<tracker<<"\n";
		vector<bool> B = T->DS.Index.B.B;
		vector<char> alpha = T->Alphabet;
		string bit_vector(""), alpha_str("");
		for(int i=0; i<B.size();i++)
			if(B[i] == 1)
				bit_vector += '1';
			else
				bit_vector += '0';
		o<<bit_vector<<"\n";
		for (int i=0; i<alpha.size(); i++)
			alpha_str += alpha[i];
		o<<alpha_str<<"\n";
		Write_Wavelet_Object(T->left, tracker+'l', o);
		Write_Wavelet_Object(T->right, tracker+'r', o);
	}

}
void Build(string ip_string, string op_path)
{
	vector<char> alphabet = get_alphabets(ip_string);
	Wavelet_Tree t;
	t.Create_Tree(ip_string, alphabet);
	ofstream op_file;
	op_file.open(op_path);
	cout<<alphabet.size()<<"\n";
	cout<<ip_string.length()<<"\n";
	Write_Wavelet_Object(t.root, string("R"), op_file);
	op_file.close();
}

node* Construct_Tree(vector<string> tree_elements, int index, node* parent, map<string, int> m)
{
 	string position = tree_elements[index];
 	string bit_vec = tree_elements[index+1];
  	string alphabet = tree_elements[index+2];
  	Rank_and_Select D(bit_vec);

  	vector<char> alpha;
  	for (int j = 0; j<alphabet.length(); j++)
  		alpha.push_back(alphabet[j]);

  	string left_child = position + 'l';
  	string right_child = position + 'r';
	parent = new node;
 	parent->Alphabet = alpha;
 	parent->DS = D;
 	
 	parent->left = NULL;
 	parent->right = NULL;
 	if (m.find(left_child) != m.end())
 		parent->left = Construct_Tree(tree_elements, m[left_child], parent->left, m);
 	if (m.find(right_child) != m.end()) 
 		parent->right = Construct_Tree(tree_elements, m[right_child], parent->right, m);
	return parent;
}

Wavelet_Tree Load_Tree(string tree_path)
{
	vector<string> tree_temp;
	string line;
	ifstream tree_file(tree_path);
	while (getline(tree_file, line))
  		tree_temp.push_back(line);
  	Wavelet_Tree t;
  	map<string,int> m;
  	for(int i=0; i<tree_temp.size(); i+=3)
  		m[tree_temp[i]] = i;
  	t.root = Construct_Tree(tree_temp, 0, t.root, m);
  	return t;
 }

char access_indices(Wavelet_Tree tree, int a)
{
	bool flag = 0;
	node* start = tree.root;
	char op;
	while(flag == 0)
	{
		int bit_val = start->DS.Index.B.B[a];
		if (start->Alphabet.size() == 1)
		{
			op = start->Alphabet[0];
			flag = 1;
		}
		else
		{
			a = start->DS.Rank(a, bit_val)-1;
			if (bit_val == 0)
				start = start->left;
			if (bit_val == 1)
				start = start->right;
		}
	}
	return op;
}

void Access(string tree_path, string index_path)
{
	Wavelet_Tree t = Load_Tree(tree_path);
	vector<int> indexes;
	string line;
	ifstream index_file(index_path);
	while (getline(index_file, line))
  		indexes.push_back(atoi(line.c_str()));
  	for (int i=0; i<indexes.size(); i++)
  		cout<<access_indices(t, indexes[i])<<"\n";
}

void Rank(string tree_path, string queries_path)
{
	Wavelet_Tree t = Load_Tree(tree_path);
	ifstream query_file(queries_path);
	string segment, line;
	while (getline(query_file, line))
	{
		stringstream temp(line);
  		vector<string> split;
  		while (std::getline(temp, segment, '\t'))
			split.push_back(segment);
		int pos = atoi(split[1].c_str());
		char search = split[0][0];
		cout<<t.Query_Rank_Wrapper(pos, search)<<"\n";
  	}
}

void Select(string tree_path, string queries_path)
{
	Wavelet_Tree t = Load_Tree(tree_path);
	ifstream query_file(queries_path);
	string segment, line;
	while (getline(query_file, line))
	{
		stringstream temp(line);
  		vector<string> split;
  		while (std::getline(temp, segment, '\t'))
			split.push_back(segment);
		int rank = atoi(split[1].c_str());
		char search = split[0][0];
		cout<<t.Query_Select_Wrapper(rank, search)<<"\n";
  	}
}

int main(int argc, char* argv[])
{
	int param_count = argc;
	string op_type = argv[2];
	if(!op_type.compare("Build"))
	{
		string ip_path = argv[3];
		string op_path = argv[4];
		std::ifstream file(ip_path);
		string ip_string;
		getline(file, ip_string);
		Build(ip_string, op_path);
	}
	if(!op_type.compare("Access"))
	{
		string tree_path = argv[3];
		string index_path = argv[4];
		Access(tree_path, index_path);
	}	
	if(!op_type.compare("Rank"))
	{
		string tree_path = argv[3];
		string query_path = argv[4];
		Rank(tree_path, query_path);
	}
	if(!op_type.compare("Select"))
	{
		string tree_path = argv[3];
		string query_path = argv[4];
		Select(tree_path, query_path);
	}
	return 0;
}