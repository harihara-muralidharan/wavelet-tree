#include"Wavelet.h"
#include <chrono>
#include <ctime>
#include<cstdlib>

int main(int argc, char* argv[])
{
	int param_count = argc;
	string op_type = argv[2];
	string query_string = argv[3];
	vector<char> alphabet = get_alphabets(query_string);

	Wavelet_Tree t;
	t.Create_Tree(query_string, alphabet);

	auto start = std::chrono::system_clock::now();
	if (!op_type.compare("Rank"))
	{
		cout<<"Rank ";
		for (int i=4; i<param_count; i+=2)
		{
			int index =  atoi(argv[i]);
			char search_char = *argv[i+1];
			int r1 = t.Query_Rank_Wrapper(index, search_char);
		}
	}
	else
	{
		cout<<"Select ";
		if (!op_type.compare("Select"))
		{
			for (int i=4; i<param_count; i+=2)
			{
				int rank =  atoi(argv[i]);
				char search_char = *argv[i+1];
				int r1 = t.Query_Select_Wrapper(rank, search_char);
			}
		}
	}
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;

	cout<<elapsed_seconds.count() << " "<<query_string.length()<<" "<<alphabet.size();
	return 0;
}