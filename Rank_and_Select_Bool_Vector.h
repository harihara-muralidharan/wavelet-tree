#include <bitset>
#include<math.h>
#include"Bitset_Bool.h"

using namespace std;
const uint64_t max_bitstr_len = 50000; 

class Rank_DS
{
	public:

		BitSet_Bool B; 
		uint64_t n;
		uint64_t s;
		uint64_t b;
		uint64_t num_super_blocks;
		uint64_t num_blocks;
		vector<uint64_t> Rp;
		vector<uint64_t> Rs;
		vector<uint64_t> Rb;

		Rank_DS()
		{

		}

		Rank_DS(string bitvec_str)
		{
			BitSet_Bool Temp(bitvec_str);
			B = Temp;

			n = bitvec_str.length();
			s =  (uint64_t) ((log2(n)*log2(n))/2);
			b =  ceil(log2(n)/2);
			num_super_blocks = ceil(n*1.0/s);
			num_blocks = ceil(s*1.0/b);

			Rp = Build_Rp(b);

			for(int j=0; j<num_super_blocks; j++)
			{
				uint64_t t = rank_iterative(B, n, j*s);
				Rs.push_back(t);
				for (int k=0; k<num_blocks; k++)
				{
					uint64_t loc_k = j*s + k*b;
					if (loc_k >= n || j != (uint64_t) loc_k/s)
						break;
					uint64_t c = rank_iterative(B, n, loc_k) - t;
					Rb.push_back(c);
				}
			}
		}

		vector<uint64_t> Build_Rp(uint64_t b)
		{
			uint64_t max_bits = pow(2,b);
			vector<uint64_t> Rp;
			for (int i = 0; i<max_bits; i++)
			{
				bitset<max_bitstr_len> bits(i);
				Rp.push_back(bits.count());
			}
			return Rp;
		}

		uint64_t rank_iterative(BitSet_Bool B, uint64_t n, uint64_t q)
		{
			if(q == 0)
				return 0;
			uint64_t rank_sum = 0;
			for(int i=0; i<q; i++)
			{
				vector<bool>::iterator it;
				it = B.B.begin()+i;
				rank_sum += (uint64_t) *it;
			}
			return rank_sum;
		}
};

class Rank_and_Select
{
	public:

		Rank_DS Index;

		Rank_and_Select()
		{

		}

		Rank_and_Select(string bit_vector)
		{
			Rank_DS R(bit_vector);
			Index = R;
		}

		uint64_t Rank(uint64_t i, uint64_t bit_value=1)
		{
			uint64_t super_block_id = (uint64_t) i/Index.s;
			uint64_t block_wrt_sb = (uint64_t) (i - super_block_id*Index.s)/Index.b;
			uint64_t block_sum = Index.Rp[Index.B.to_ulong(Index.s*super_block_id + block_wrt_sb*Index.b, i+1)];
			uint64_t R_1 = Index.Rs[super_block_id] + Index.Rb[Index.num_blocks*super_block_id + block_wrt_sb] + block_sum;

			if (bit_value == 1)
				return R_1;
			else
				return i - R_1+1;
		}

		uint64_t Select(uint64_t i, uint64_t L, uint64_t U, uint64_t search)
		{
			uint64_t mid = ((L+U)*0.5);

			uint64_t mid_rank = Rank(mid, search);
			vector<bool>::iterator it;
			it = Index.B.B.begin()+mid;
			if (i > U)
				return -1;
			else
			{
				if (mid_rank == i and *it == search)
					return mid;
				else
					if (mid_rank >= i)
						return Select(i, L, mid, search);
					else
						return Select(i, mid+1, U, search);
			}
		}
};