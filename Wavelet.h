#include "Rank_and_Select_Bool_Vector.h"
#include <map>
#include <algorithm> 

struct node
{
	vector<char> Alphabet;
	node* left;
	node* right;
	Rank_and_Select DS;
};

class Wavelet_Tree
{
	public:
		node* root;

		Wavelet_Tree()
		{
			root = NULL;
		}

		node* Build_Tree(string s, vector<char> alphabet, node* T)
		{
			if (alphabet.size() == 1)
			{
				string bitstr("");
				for (int i = 0; i<s.length(); i++)
					bitstr += "1";
				Rank_and_Select D(bitstr);

				T = new node;
				T->Alphabet = alphabet;
				T->DS = D;
				T->left = NULL;
				T->right = NULL;
				return T;
			}
			else
			{
				T = new node;
				int alpha_size = alphabet.size(), string_length = s.length(), ctr = 0;
				string bitstr(""), lst(""), rst("");
				std::map<char, int> temp_alphabet_map;
				vector<char> mask_0, mask_1;
				
				for(vector<char>::iterator i = alphabet.begin(); i != alphabet.end(); ++i)
				{ 
					temp_alphabet_map[*i] = ctr;
					if (ctr < ceil(alpha_size*0.5))
						mask_0.push_back(*i);
					else
						mask_1.push_back(*i);
					ctr += 1;
				}

				for(int i = 0; i< string_length; i++)
					if (temp_alphabet_map[s[i]] < ceil(alpha_size*0.5))
					{
						bitstr += '0';
						lst += s[i];
					}
					else
					{
						bitstr += '1';
						rst += s[i];
					}

				Rank_and_Select D(bitstr);

				T->DS = D;
				T->Alphabet = alphabet;
				T->left = Build_Tree(lst, mask_0, T->left);
				T->right = Build_Tree(rst, mask_1, T->right);

				return T;
			}
		}

		void Create_Tree(string s, vector<char> alphabet)
		{
			root = Build_Tree(s, alphabet, root);
		}

		uint64_t Query_Rank_Wrapper(uint64_t pos, char c)
		{
			return Query_Rank(pos, c, root);
		}

		uint64_t Query_Rank(uint64_t index, char c, node* T)
		{
			uint64_t i, rank;
			while(T->left != NULL && T->right != NULL)
			{
				for (i = 0;i < T->Alphabet.size(); i++)
					if (T->Alphabet[i] == c)
						break;

				if (i < ceil(T->Alphabet.size()*0.5))
				{
					rank = T->DS.Rank(index, 0);
					T = T->left;
					index = rank-1;
				}
				else
				{
					rank = T->DS.Rank(index, 1);
					T = T->right;
					index = rank-1;
				}
			}
		 	return rank;
		}

		uint64_t Query_Select_Wrapper(uint64_t rank, char c)
		{
			return Query_Select(rank, c, root);
		}

		uint64_t Query_Select(uint64_t rank, char c, node* T)
		{

			if 	(T->Alphabet.size() == 1)
				return T->DS.Select(rank, 0, T->DS.Index.n, 1);

			uint64_t B,index,i;

			for (i = 0;i < T->Alphabet.size(); i++)
				if (T->Alphabet[i] == c)
					break;

			if (i < ceil(T->Alphabet.size()*0.5))
			{
				B = 0;
				index = Query_Select(rank, c, T->left);
			}
			else
			{
				B = 1;
				index = Query_Select(rank, c, T->right);
			}
			return T->DS.Select(index+1, 0, T->DS.Index.n, B);
		}

};

vector<char> get_alphabets(string s)
{
	vector<char> alphabet (s.begin(), s.end());
	vector<char> uniq_chars;
	sort(alphabet.begin(), alphabet.end());
	uniq_chars.push_back(alphabet[0]);
	for (int i = 1; i<alphabet.size(); i++)
		if(alphabet[i-1] != alphabet[i])
			uniq_chars.push_back(alphabet[i]);
	return uniq_chars;
}
