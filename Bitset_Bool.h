#include <iostream>
#include <string>
#include<vector>
#include <numeric>     

using namespace  std;

long to_int(long x, bool y)
{
    return (x<<1)+y;
}

class BitSet_Bool
{
	public:
		vector<bool> B;

		BitSet_Bool()
		{

		}

		BitSet_Bool(string S)
		{
			for(int i=0; i<S.length(); i++)
				B.push_back((uint64_t) S[i] - (uint64_t) '0');
		}

		uint64_t to_ulong(uint64_t start, uint64_t end)
		{
			vector<bool>::iterator it1, it2;
    		it1 = B.begin() + start;
    		it2 = B.begin() + end;
    		return accumulate(it1, it2, 0L, to_int);
		}
};